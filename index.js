require('dotenv').config();

const bodyParser = require('body-parser');
const morgan = require('morgan');
const cors = require('cors');
let axios = require('axios');

const express = require('express');
const app = express();

app.use(cors());

app.use(morgan('dev'));
app.use(bodyParser.json());

// routes
app.get('/', async (req, res) => {
  const { q } = req.query;
  let params = {
    part: 'snippet',
    key: process.env.API_KEY,
    q,
    maxResults: 25,
    type: 'video',
  };

  const result = await axios
    .get(process.env.BASE_URL, { params })
    .then((response) => {
      return response;
    });
  if (result.status == 200) {
    res.json({
      status: true,
      data: result.data.items,
      message: 'ok',
    });
  } else {
    res.json({
      status: false,
      data: null,
      message: 'error',
    });
  }
});

app.use((req, res, next) => {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

app.listen(process.env.PORT, () => {
  console.log(`server on port ${process.env.PORT}`);
});
